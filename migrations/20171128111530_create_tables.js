exports.up = function(knex, Promise) {
    return knex.schema.createTable('properties', function(table) {
        table.increments('id').primary();
        table.string('airbnb_id').notNullable().index();
        table.unique(['airbnb_id']);
    })

    .createTable('addresses', (table) => {
        table.increments('id').primary();
        table.string('line1').notNullable();
        table.string('line2');
        table.string('line3');
        table.string('line4').notNullable();
        table.string('postcode').notNullable();
        table.string('city').notNullable();
        table.string('country').notNullable();
        table.integer('property_id').unsigned().references('id').inTable('properties').onDelete('CASCADE');
    })

    .createTable('property_versions', function(table) {
        table.increments('id').primary()
        table.string('host').notNullable().index()
        table.integer('property_id').unsigned().references('id').inTable('properties').onDelete('CASCADE');
        table.integer('number_of_bedrooms').notNullable()
        table.integer('number_of_bathrooms').notNullable()
        table.float('generated_income').notNullable()
        table.timestamps()
    })
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists("addresses"),
        knex.schema.dropTableIfExists("property_versions"),
        knex.schema.dropTableIfExists("properties")
    ]);
};