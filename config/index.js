const convict = require('convict');
const configSchema = require('./schema');

// Load config schema
const config = convict(configSchema);

// Load environment dependending on the enabled configuration
const env = config.get('env');

// Load env vars from json file.
// Config vars assignment precedence from lowest(1) to highest(5) are:
//  1. Default value
//  2. File (config.loadFile())
//  3. Environment variables
//  4. Command line arguments
//  5. Set and load calls (config.set() and config.load())
config.loadFile('./config/envs/' + env + '.json');

// Perform validation
config.validate({ allowed: 'strict' });

module.exports = config;