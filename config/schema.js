// Config general schema
const configSchema = {
    env: {
        doc: 'The applicaton environment.',
        format: ['production', 'staging', 'development', 'test'],
        default: 'development',
        env: 'NODE_ENV'
    },
    server: {
        host: {
            doc: 'The IP address to bind.',
            format: 'ipaddress',
            default: '0.0.0.0',
            env: 'API_IP_ADDRESS_BIND'
        },
        port: {
            doc: 'The port to bind.',
            format: 'port',
            default: 8080,
            env: 'API_PORT'
        }
    },
    knex: {
        client: {
            doc: 'Database client.',
            format: String,
            default: 'mysql',
            env: 'API_KNEX_DB_CLIENT'
        },
        connection: {
            database: {
                doc: 'Database names.',
                format: String,
                default: 'properties',
                env: 'API_KNEX_DB_NAME'
            },
            host: {
                doc: 'The DB host.',
                format: 'ipaddress',
                default: '127.0.0.1',
                env: 'API_KNEX_DB_HOST'
            },
            user: {
                doc: 'Database user.',
                format: String,
                default: 'root',
                env: 'API_KNEX_DB_USER'
            },
            password: {
                doc: 'Database password.',
                format: String,
                default: '',
                env: 'API_KNEX_DB_PASSWORD'
            }
        },
        pool: {
            min: {
                doc: 'Minimun number of connections.',
                format: 'nat',
                default: 2,
                env: 'API_KNEX_DB_POOL_MIN_CONNECTIONS'
            },
            max: {
                doc: 'Max number of connection.',
                format: 'nat',
                default: 10,
                env: 'API_KNEX_DB_POOL_MAX_CONNECTIONSS'
            }
        }
    },
    logger: {
        name: {
            doc: 'API logger name',
            format: String,
            default: 'Property-Manager-API'
        },
        level: {
            doc: 'Logger level',
            format: ['trace', 'debug', 'info', 'warn', 'error', 'fatal'],
            default: 'trace'
        }
    },
    airbnb: {
        validationUrl: {
            doc: 'Airbnb validation Url.',
            format: String,
            default: 'https://www.airbnb.co.uk/rooms/',
            env: 'API_AIRBNB_VALIDATION_URL'
        }
    }
};

module.exports = configSchema;