'use strict'

const hapiSwagger = require('hapi-swagger');

module.exports = {
    register: hapiSwagger,
    options: {
        info: {
            title: 'properties-manager API Documentation',
            version: '1.0.0'
        }
    }
};