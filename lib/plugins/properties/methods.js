const async = require('async');
const log = require('lib/util/logger');
const bookshelf = require('lib/db');
const Address = require('lib/models/Address');
const Property = require('lib/models/Property');
const PropertyVersion = require('lib/models/PropertyVersion');

const methods = {};

/**
 * Create a property and the respective related data
 * 
 * @param {*} propertyData 
 * @param {*} callback 
 */
methods.createProperty = (propertyData, callback) => {

    // Create Address, Property and PropertyVersion in a single transaction.
    // If one one operation fails it rollback automaticaly
    bookshelf.transaction((transaction) => {
        return Property.create({ airbnb_id: propertyData.airbnbId }, { transacting: transaction }).then((property) => {
            const addressData = Object.assign({ property_id: property.attributes.id },
                propertyData.address
            );

            return Address.create(addressData, { transacting: transaction }).then((address) => {
                property.related('address').set(address.attributes);
                const propertyVersionObj = {
                    host: propertyData.host,
                    property_id: property.attributes.id,
                    number_of_bedrooms: propertyData.numberOfBedrooms,
                    number_of_bathrooms: propertyData.numberOfBathrooms,
                    generated_income: propertyData.generatedIncome
                };

                return PropertyVersion.create(propertyVersionObj, { transacting: transaction }).then((propertyVersion) => {
                    property.related('versions').set([propertyVersion.attributes]);
                    return property;
                });
            })
        })
    }).asCallback(callback);
};

/**
 * Lists all existing properties
 * 
 * @param {*} callback 
 */
methods.list = (callback) => Property.findAllWithLastVersion().asCallback(callback);

/**
 * Get a specific property by id
 * 
 * @param {*} id 
 * @param {*} callback 
 */
methods.get = (id, callback) => Property.findOneByIdWithLastVersion(id).asCallback(callback);

/**
 * Removes a specific property by id
 * 
 * @param {*} id 
 * @param {*} callback 
 */
methods.delete = (id, callback) => {
    Property.findOne({ id: id }).asCallback((err, property) => {
        if (err) return callback(err);
        if (!property) return callback(new Error('NOT_FOUND'));

        property.destroy().asCallback(callback)
    });
};

/**
 * Updates a property by id
 * 
 * @param {*} id 
 * @param {*} callback 
 */
methods.update = (id, propertyUpdate, callback) => {

    // Update in transaction
    bookshelf.transaction((transaction) => {
        return Property.findOneByIdWithLastVersion(id)
            .then((property) => {

                return property.set({ airbnb_id: propertyUpdate.airbnbId }).save(null, { transacting: transaction })
                    .then(() => {
                        const addressData = Object.assign({ property_id: property.attributes.id },
                            propertyUpdate.address
                        );

                        return property.related('address').set(addressData).save(null, { transacting: transaction })
                            .then((address) => {
                                property.related('address').set(address.attributes);
                                const propertyVersionObj = {
                                    host: propertyUpdate.host,
                                    property_id: property.get('id'),
                                    number_of_bedrooms: propertyUpdate.numberOfBedrooms,
                                    number_of_bathrooms: propertyUpdate.numberOfBathrooms,
                                    generated_income: propertyUpdate.generatedIncome
                                };

                                if (property.currentVersion().isDifferent(propertyVersionObj)) {
                                    return PropertyVersion.create(propertyVersionObj, { transacting: transaction })
                                        .then((version) => {
                                            property.related('versions').set([version]);
                                            return property;
                                        });
                                } else {
                                    return property;
                                }
                            })
                    })
            })
    }).asCallback(callback);
};

/**
 * Retrieves versions of a specific property
 * 
 * @param {*} id 
 * @param {*} callback 
 */
methods.getPropertyVersions = (id, callback) => {
    Property.findOne({ id: id }, {
        withRelated: ['address', 'versions']
    }).asCallback(callback);
};

module.exports = methods;