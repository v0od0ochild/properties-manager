const Boom = require('boom');
const Joi = require('joi');
const async = require('async');
const log = require('lib/util/logger');
const methods = require('./methods');
const preConditions = require('./pre');

module.exports = {};

/**
 * Handler for POST /properties
 */
module.exports.create = {
    validate: {
        payload: {
            host: Joi.string().required(),
            address: Joi.object().keys({
                line1: Joi.string().required(),
                line2: Joi.string(),
                line3: Joi.string(),
                line4: Joi.string().required(),
                postcode: Joi.string().required(),
                city: Joi.string().required(),
                country: Joi.string().required()
            }),
            numberOfBedrooms: Joi.number().integer().min(0).required(),
            numberOfBathrooms: Joi.number().integer().min(0).required(),
            airbnbId: Joi.string().required(),
            generatedIncome: Joi.number().positive().required()
        }
    },
    pre: [
        // Takes a bit to execute since it will test Airbnb ID
        { method: preConditions.isValidAirbnbId }
    ],
    handler: (request, reply) => {
        methods.createProperty(request.payload, (err, property) => {
            if (err) {
                log.error({ err }, 'Failed to create property');
                if (err.code && err.code === 'ER_DUP_ENTRY')
                    return reply(Boom.badRequest('Airbnb id already exists'));

                return reply(Boom.internal());
            }

            reply(property.toJSON()).created();
        });
    },
    description: 'POST /properties',
    notes: 'Creates a new property',
    tags: ['api']
};

/**
 * Handler for GET /properties/{id}
 */
module.exports.get = {
    validate: {
        params: {
            id: Joi.string().required()
        }
    },
    handler: (request, reply) => {
        methods.get(request.params.id, (err, property) => {
            if (err) return reply(Boom.internal());
            if (!property) return reply(Boom.notFound('Property doesn\'t exist'));
            reply(property.toJSON());
        });
    },
    description: 'GET /properties/{id}',
    notes: 'Retrieves an existing property',
    tags: ['api']
};

/**
 * Handler for GET /properties/{id}/evolution
 * 
 * Retrieves the evolution of property versions
 */
module.exports.getEvolution = {
    validate: {
        params: {
            id: Joi.string().required()
        }
    },
    handler: (request, reply) => {
        methods.getPropertyVersions(request.params.id, (err, property) => {
            if (err) {
                if (err.literalCode && err.literalCode === 'NOT_FOUND')
                    return reply(Boom.notFound('Property not found'));

                return reply(Boom.internal());
            }
            reply(property.displayAllVersions());
        });
    },
    description: 'GET /properties/{id}/versions',
    notes: 'Retrieves a property with all its versions',
    tags: ['api']
};

/**
 * Handler for GET /properties
 */
module.exports.list = {
    handler: (request, reply) => {
        methods.list((err, properties) => {
            if (err) return reply(Boom.internal());
            reply(properties.toJSON());
        });
    },
    description: 'GET /properties',
    notes: 'Retrieves a list of existing property',
    tags: ['api']
};

/**
 * Handler for DELETE /properties/{id}
 */
module.exports.delete = {
    validate: {
        params: {
            id: Joi.string().required()
        }
    },
    handler: (request, reply) => {
        methods.delete(request.params.id, (err) => {
            if (err) {
                if (err.message && err.message === 'NOT_FOUND')
                    return reply(Boom.notFound('Property not found'));

                return reply(Boom.internal());
            }
            reply().code(204);
        });
    },
    description: 'DELETE /properties',
    notes: 'Deletes an existing property',
    tags: ['api']
};

/**
 * Handler for UPDATE /properties/{id}
 * 
 * Updates/Replaces property details, and if generated income changes, 
 * a new line is inserted to keep track of changes
 */
module.exports.update = {
    validate: {
        payload: {
            host: Joi.string().required(),
            address: Joi.object().keys({
                line1: Joi.string().required(),
                line2: Joi.string(),
                line3: Joi.string(),
                line4: Joi.string().required(),
                postcode: Joi.string().required(),
                city: Joi.string().required(),
                country: Joi.string().required()
            }),
            numberOfBedrooms: Joi.number().integer().min(0).required(),
            numberOfBathrooms: Joi.number().integer().min(0).required(),
            airbnbId: Joi.string().required(),
            generatedIncome: Joi.number().positive().required()
        }
    },
    pre: [
        // Takes a bit to execute since it will test Airbnb ID
        { method: preConditions.isValidAirbnbId }
    ],
    handler: (request, reply) => {
        methods.update(request.params.id, request.payload, (err, updatedProperty) => {
            if (err) {
                if (err.literalCode && err.literalCode === 'NOT_FOUND')
                    return reply(Boom.notFound('Property not found'));

                return reply(Boom.internal());
            }
            reply(updatedProperty.toJSON());
        });
    },
    description: 'PUT /properties/{id}',
    notes: 'Updates a property. If property version data changes, a new one is created',
    tags: ['api']
};