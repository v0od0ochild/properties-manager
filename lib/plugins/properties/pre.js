const request = require('request');
const config = require('config');
const log = require('lib/util/logger');
const Boom = require('boom');

// Preconditions to be exported
const preConditionMethods = {};

// Airbnb IDs must be validated in the website
preConditionMethods.isValidAirbnbId = (req, reply) => {
    request.get({
        url: config.get('airbnb').validationUrl + req.payload.airbnbId,
        followRedirect: false,
        headers: {
            'user-agent': 'Mozilla / 5.0(Macintosh; Intel Mac OS X 10 _12_6) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 62.0 .3202 .94 Safari / 537.36'
        }
    }, (err, response, body) => {
        if (err) {
            logger.warn({ err }, 'Failed to GET airbnb room');
            return reply(Boom.internal());
        }
        if (response.statusCode !== 200) {
            log.warn({ statusCode: response.statusCode }, 'Airbnb ID is invalid');
            return reply(Boom.badRequest('AirbnbId isn\t valid'));
        }
        return reply();
    });
};

/**
 * Some thoughts about handling the request limits
 * 
 * One option would be not having the airbnbID validation in real time. If that validation doen't require to
 * be instant, we could add an extra field to the property model, for instance `validated`, which would 
 * default to false. Then create some sentinel that would pick up a batch of properties and try once in a while
 * to validate the ids using a backoff algorythm. When a request is non blocked, we can then confirm if we can
 * enable it or possibly delete it.
 * 
 * Another solution to help minimizing the issue would be having cache, a redis for instance, where successfuly
 * validated ids would be inserted into a registry, having a time to live. When in need of validating an id that
 * registry should be consulted first.
 */
module.exports = preConditionMethods;