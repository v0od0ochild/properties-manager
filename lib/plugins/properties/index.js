const handlers = require('./handlers');
const preConditions = require('./pre');

exports.register = (server, options, next) => {
    // Registering plugin routes
    server.route({
        method: 'POST',
        path: '/properties',
        config: handlers.create
    });

    server.route({
        method: 'GET',
        path: '/properties',
        config: handlers.list
    });

    server.route({
        method: 'GET',
        path: '/properties/{id}',
        config: handlers.get
    });

    server.route({
        method: 'DELETE',
        path: '/properties/{id}',
        config: handlers.delete
    });

    server.route({
        method: 'PUT',
        path: '/properties/{id}',
        config: handlers.update
    });

    server.route({
        method: 'GET',
        path: '/properties/{id}/versions',
        config: handlers.getEvolution
    });

    next();
};

// Register plugin attributes name and version
exports.register.attributes = {
    name: 'Properties',
    version: '1.0.0'
};