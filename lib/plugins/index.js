// Load plugins
const inert = require('inert');
const vision = require('vision');
const swaggerPlugin = require('lib/plugins/internal/swagger');
const propertiesPlugin = require('lib/plugins/properties');

// List of plugins to be exported
module.exports = [
    inert, // Required by swagger
    vision, // Required by swagger
    swaggerPlugin,
    propertiesPlugin
];