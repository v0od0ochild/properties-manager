const BaseModel = require('lib/models/BaseModel');

class Address extends BaseModel {
    /**
     * Getter
     */
    get tableName() { return 'addresses'; }

    /**
     * Serializer
     */
    serialize() {
        return {
            line1: this.get('line1'),
            line2: this.get('line2'),
            line3: this.get('line3'),
            line4: this.get('line4'),
            postcode: this.get('postcode'),
            city: this.get('city'),
            country: this.get('country')
        }
    }

    /**
     * Property relationship reference
     */
    /**property() {
        return this.belongsTo('Property');
    }*/

    /**
     * Detect changes
     * 
     * @param {*} obj 
     */
    isDifferent(obj) {
        const comparisonTerms = ['line1', 'line2', 'line3', 'line4', 'postcode', 'city', 'country'];
        for (let term of comparisonTerms) {
            if (this.get(term) !== obj[term]) return true;
        }
        return false;
    }
}

module.exports = Address;