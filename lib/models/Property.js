const BaseModel = require('lib/models/BaseModel');
const PropertyVersion = require('lib/models/PropertyVersion');
const Address = require('lib/models/Address');
const knex = require('lib/knex');

/**
 * This model represents a Property.
 * It's composed by the airbnbID and multiple PropertyVersions
 */
class Property extends BaseModel {

    /**
     * Getter to retrieve Property model table name
     */
    get tableName() { return 'properties'; }

    /**
     * Retrieves a Model instance by ID and with the relationships attached 
     * and only the most recent property version
     * 
     * Queries DB using knex query builder (with SQL injection escaping)
     * 
     * @param {*} id 
     */
    static findOneByIdWithLastVersion(id) {
        // Bookshelf was doing 3 queries separatelly
        // Replacing it with the following query and rebuilding the model
        return knex('properties').select(
                'addresses.id as address_id',
                'addresses.line1',
                'addresses.line2',
                'addresses.line3',
                'addresses.line4',
                'addresses.postcode',
                'addresses.city',
                'addresses.country',
                'properties.id as property_id',
                'properties.airbnb_id',
                'property_versions.id as property_versions_id',
                'property_versions.host',
                'property_versions.number_of_bedrooms',
                'property_versions.number_of_bathrooms',
                'property_versions.generated_income',
                'property_versions.created_at',
                'property_versions.updated_at'
            )
            .where('properties.id', '=', id)
            .join('addresses', 'addresses.property_id', 'properties.id')
            .join('property_versions', 'property_versions.property_id', 'properties.id')
            .orderBy('property_versions.created_at', 'DESC')
            .limit(1)
            .then((results) => {
                if (!results || !results.length || results.length && results.length === 0) return;

                const data = results[0];
                const property = new Property({
                    id: data.property_id,
                    airbnb_id: data.airbnb_id
                });

                property.related('address').set({
                    id: data.address_id,
                    line1: data.line1,
                    line2: data.line2,
                    line3: data.line3,
                    line4: data.line4,
                    postcode: data.postcode,
                    city: data.city,
                    country: data.country
                });

                property.related('versions').set([{
                    id: data.property_versions_id,
                    host: data.host,
                    number_of_bedrooms: data.number_of_bedrooms,
                    number_of_bathrooms: data.number_of_bathrooms,
                    generated_income: data.generated_income,
                    created_at: data.created_at,
                    updated_at: data.updated_at
                }]);

                return property;
            })
    }

    /**
     * Retrieves a Collection of Models with the relationships attached 
     * and only the most recent property version
     * 
     */
    static findAllWithLastVersion() {
        return knex.raw(`SELECT
            a.id,
            a.airbnb_id,
            b.property_versions_id,
            b.host,
            b.number_of_bedrooms,
            b.number_of_bathrooms,
            b.generated_income,
            b.created_at,
            b.updated_at,
            addresses.id as address_id,
            addresses.line1,
            addresses.line2,
            addresses.line3,
            addresses.line4,
            addresses.postcode,
            addresses.city,
            addresses.country FROM properties a JOIN(
                SELECT v1.id as property_versions_id,
                v1.host,
                v1.property_id,
                v1.number_of_bedrooms,
                v1.number_of_bathrooms,
                v1.generated_income,
                v1.created_at,
                v1.updated_at FROM property_versions v1 JOIN(
                    SELECT property_versions.property_id, MAX(created_at) As maxDate FROM property_versions GROUP BY property_id
                ) v2 ON v1.property_id = v2.property_id AND v1.created_at = v2.maxDate
            ) b ON a.id = b.property_id JOIN addresses ON addresses.property_id = b.property_id;`)
            .then((results) => {
                if (!results || !results.length || !results[0].length) return [];

                const collection = Property.collection();

                for (let data of results[0]) {
                    const property = new Property({
                        id: data.id,
                        airbnb_id: data.airbnb_id
                    });

                    property.related('address').set({
                        id: data.address_id,
                        line1: data.line1,
                        line2: data.line2,
                        line3: data.line3,
                        line4: data.line4,
                        postcode: data.postcode,
                        city: data.city,
                        country: data.country
                    });

                    property.related('versions').set([{
                        id: data.property_versions_id,
                        host: data.host,
                        number_of_bedrooms: data.number_of_bedrooms,
                        number_of_bathrooms: data.number_of_bathrooms,
                        generated_income: data.generated_income,
                        created_at: data.created_at,
                        updated_at: data.updated_at
                    }]);

                    collection.add(property);
                }
                return collection;
            })
    }


    /**
     * Retrieves the latest property version from 
     */
    currentVersion() {
        return this.related('versions').orderBy('created_at', 'DESC').at(0);
    }

    /**
     * Maps address relationship
     */
    address() {
        return this.hasOne(Address);
    }

    /**
     * Maps versions relationship
     */
    versions() {
        return this.hasMany(PropertyVersion);
    }

    /**
     * Changes validator
     * 
     * @param {*} obj 
     */
    isDifferent(obj) {
        return this.get('airbnb_id') === obj.airbnb_id;
    }

    /**
     * JSON presenter to display a property details with current version
     */
    toJSON() {
        return Object.assign({
            id: this.get('id'),
            airbnbId: this.get('airbnb_id'),
            address: this.related('address').serialize()
        }, this.currentVersion().serialize());
    }

    /**
     * JSON presenter to display the property details with all versions
     */
    displayAllVersions() {
        return {
            id: this.get('id'),
            airbnbId: this.get('airbnb_id'),
            address: this.related('address').serialize(),
            versions: this.related('versions').serialize()
        };
    }
}

module.exports = Property;