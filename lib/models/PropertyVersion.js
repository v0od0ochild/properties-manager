const BaseModel = require('lib/models/BaseModel');

/**
 * 
 * This model represents the property changes(versions) over time,
 * It's not explicit which properties should be versioned, so I am assuming its these ones
 */
class PropertyVersion extends BaseModel {
    /**
     * Getters
     */
    get tableName() { return 'property_versions'; }
    get hasTimestamps() { return true; }

    /**
     * Serializer
     */
    serialize() {
        return {
            host: this.get('host'),
            numberOfBedrooms: this.get('number_of_bedrooms'),
            numberOfBathrooms: this.get('number_of_bathrooms'),
            generatedIncome: this.get('generated_income')
        }
    }

    /**
     * Property relationship
     */
    property() {
        return this.belongsTo('Property');
    }

    /**
     * Performs a validation to check if the update requires a new
     * 
     * @param {*} obj 
     */
    isDifferent(obj) {
        const comparisonTerms = ['host', 'number_of_bedrooms', 'number_of_bathrooms', 'income_generated'];
        for (let term of comparisonTerms) {
            if (this.get(term) !== obj[term]) return true;
        }
        return false;
    }
}

module.exports = PropertyVersion;