const bookshelf = require('lib/db');

/**
 * Common Class methods to be extende by the other models.
 * Keeping it DRY
 */
class BaseModel extends bookshelf.Model {

    /**
    static findAll(filter, options) {
        return this.forge().where(filter).fetchAll(options);
    }
    */

    static findOne(query, options) {
        return this.forge(query).fetch(options);
    }

    static create(data, options) {
        return this.forge(data).save(null, options);
    }

}

module.exports = BaseModel;