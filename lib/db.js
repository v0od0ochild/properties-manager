const Knex = require('lib/knex');
const bookshelf = require('bookshelf')(Knex)

bookshelf.plugin('registry');

module.exports = bookshelf;