const bunyan = require('bunyan');
const config = require('config');

// Create logger loading logger config
const logger = bunyan.createLogger(config.get('logger'));

module.exports = logger;