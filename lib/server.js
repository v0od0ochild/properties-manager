const Hapi = require('hapi');
const config = require('config');
const log = require('lib/util/logger');

// Create a new Hapi Server instance
const server = new Hapi.Server();

// Setup server based on config file
server.connection(config.get('server'));

module.exports = server;