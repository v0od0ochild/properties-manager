const server = require('lib/server');
const log = require('lib/util/logger');
const config = require('config');

log.trace('Starting Property Manager API (%s)', config.get('env'));

// Load plugins
const plugins = require('lib/plugins');

// Register loaded plugins on server
server.register(plugins, (err) => {
    if (err) {
        log.fatal(err, 'Hapi failed to register plugins');
        throw (err);
    }

    log.trace('All server plugins loaded successfuly');

    // Start server
    server.start((err) => {
        if (err) {
            log.fatal(err, 'Failed to start server');
            throw err;
        }

        log.info('API up and running on port %s', config.get('server').port);
    });
});