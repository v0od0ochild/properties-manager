const { describe, it } = exports.lab = require('lab').script();
const expect = require('chai').expect;
const server = require('lib/server');

describe("PUT /properties/{id}", () => {

    const originalPayload = {
        host: 'Miguel',
        airbnbId: '3993887',
        numberOfBedrooms: 2,
        numberOfBathrooms: 1,
        generatedIncome: 2000.50,
        address: {
            line1: 'Addr line1',
            line2: 'Addr line2',
            line3: 'Addr line3',
            line4: 'Addr line4',
            postcode: '1234-567',
            city: 'Montijo',
            country: 'Portugal'
        }
    };

    const newPayload = {
        host: 'changed_host',
        airbnbId: '3882809',
        numberOfBedrooms: 5,
        numberOfBathrooms: 8,
        generatedIncome: 1000.50,
        address: {
            line1: 'changed_line1',
            line2: 'changed_line2',
            line3: 'changed_line3',
            line4: 'changed_line4',
            postcode: '7654-321',
            city: 'changed_city',
            country: 'changed_country'
        }
    };

    it("should be able to update the property", async() => {

        const req = {
            method: 'PUT',
            url: '/properties/1',
            payload: newPayload
        };

        const res = await server.inject(req);
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.be.an('object');
        expect(res.result.id).to.equal(1);
        const expectedObj = Object.assign({ id: 1 }, newPayload);
        expect(res).to.have.deep.property('result', expectedObj);
    });

    it("should have created a new property version", async() => {
        const req = {
            method: 'GET',
            url: '/properties/1/versions'
        };

        const res = await server.inject(req);
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.be.an('object');
        expect(res.result.id).to.equal(1);
        expect(res.result.versions).to.be.an('array');
        expect(res.result.versions).to.have.lengthOf(2);
    });
});