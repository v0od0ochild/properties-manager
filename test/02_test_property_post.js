const { describe, it } = exports.lab = require('lab').script();
const expect = require('chai').expect;
const server = require('lib/server');

describe("POST /properties", () => {

    const payload = {
        host: 'Miguel',
        airbnbId: '3993887',
        numberOfBedrooms: 2,
        numberOfBathrooms: 1,
        generatedIncome: 2000.50,
        address: {
            line1: 'Addr line1',
            line2: 'Addr line2',
            line3: 'Addr line3',
            line4: 'Addr line4',
            postcode: '1234-567',
            city: 'Montijo',
            country: 'Portugal'
        }
    };

    it("should return 201 when property is created", async() => {
        const req = {
            method: 'POST',
            url: '/properties',
            payload: payload
        };

        const res = await server.inject(req);
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.be.an('object');
        expect(res.result).to.have.property('id');
        let expectedPayload = Object.assign({ id: res.result.id }, payload);
        expect(res).to.have.deep.property('result', expectedPayload);
    });


    it("Should return 400 when airbnb id is invalid", async() => {
        const reqPayload = Object.assign({ airbnbId: '00000000' }, payload);
        const req = {
            method: 'POST',
            url: '/properties',
            payload: reqPayload
        };

        const res = await server.inject(req);
        expect(res.statusCode).to.equal(400);
    });
});