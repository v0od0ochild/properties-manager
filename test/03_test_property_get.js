const { describe, it } = exports.lab = require('lab').script();
const expect = require('chai').expect;
const server = require('lib/server');

describe("GET /properties/{id}", () => {

    const payload = {
        id: 1,
        host: 'Miguel',
        airbnbId: '3993887',
        numberOfBedrooms: 2,
        numberOfBathrooms: 1,
        generatedIncome: 2000.50,
        address: {
            line1: 'Addr line1',
            line2: 'Addr line2',
            line3: 'Addr line3',
            line4: 'Addr line4',
            postcode: '1234-567',
            city: 'Montijo',
            country: 'Portugal'
        }
    };

    it("should return 200 if property is found", async() => {

        const req = {
            method: 'GET',
            url: '/properties/1'
        };

        const res = await server.inject(req);
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.be.an('object');
        expect(res.result).to.have.property('id');
        expect(res).to.have.deep.property('result', payload);
    });


    it("Should return a 404 when property is not found", async() => {
        const req = {
            method: 'GET',
            url: '/properties/2'
        };

        const res = await server.inject(req);
        expect(res.statusCode).to.equal(404);
    });

    describe("GET /properties/{id}/versions", () => {

        it("should be able to retrieve all property versions", async() => {

            const req = {
                method: 'GET',
                url: '/properties/1/versions'
            };

            const res = await server.inject(req);
            expect(res.statusCode).to.equal(200);
            expect(res.result).to.be.an('object');
            expect(res.result).to.have.property('id');
            expect(res.result).to.have.property('versions');
            expect(res.result.versions).to.be.an('array');
        });
    });
});