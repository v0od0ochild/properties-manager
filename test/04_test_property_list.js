const { describe, it } = exports.lab = require('lab').script();
const expect = require('chai').expect;
const server = require('lib/server');

describe("GET /properties", () => {

    it("should return 200 with an array of properties", async() => {

        const req = {
            method: 'GET',
            url: '/properties'
        };

        const res = await server.inject(req);
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.be.an('array');
    });
});