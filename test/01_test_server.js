const { describe, it } = exports.lab = require('lab').script();
const expect = require('chai').expect;
const server = require('lib/server');
const plugins = require('lib/plugins');

describe("Server plugins", () => {

    it("should load all API plugins successfuly", () => {
        server.register(plugins, (err) => {
            expect(err).to.be.undefined;
        });
    });

    it("server should have more than one route available", () => {
        const serverTable = server.table();
        expect(serverTable[0].table).to.have.lengthOf.at.least(1);
    });
});