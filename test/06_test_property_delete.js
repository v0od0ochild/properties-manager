const { describe, it } = exports.lab = require('lab').script();
const expect = require('chai').expect;
const server = require('lib/server');

describe("DELETE /properties/{id}", () => {

    it("should return 204 if property is deleted", async() => {

        const req = {
            method: 'DELETE',
            url: '/properties/1'
        };

        const res = await server.inject(req);
        expect(res.statusCode).to.equal(204);
    });

    it("should return 404 if property was deleted and no longer exists", async() => {

        const req = {
            method: 'DELETE',
            url: '/properties/1'
        };

        const res = await server.inject(req);
        expect(res.statusCode).to.equal(404);
    });
});