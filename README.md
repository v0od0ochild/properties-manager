# property-manager

An exercise to manage properties

## Installing

Run `npm i` to install all dependencies. Tested with node 8.9.


## ENVs

All available envs are displayed inside `config/schema.js` file. Default NODE_ENV is development.


## Creating database structure and migrating

This project uses MySql.
To create database run `npm run createDb`, following test env config file (`config/envs/test.json`). `npm run dropDb` will drop the database.

With the db created, run `npm run migrate` to create the tables in the databses.


## Running

Run `npm start` or `npm start | bunyan` for bunyan logs.


## Debugging

To be able to log mysql queries start  the server with `npm run debug`-

## Docs

To be able to access swagger documentation, start the server(e.g. `npm start`) and open `http://localhost:3000/documentation` in a browser.

## Tests

`config/envs/test.json` file, holds the testing settings.
Run `npm test` to run the tests.