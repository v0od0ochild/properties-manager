const config = require('config');

// connect without database selected
var knex = require('knex')({
    client: config.get('knex').client,
    connection: {
        host: config.get('knex').connection.host,
        user: config.get('knex').connection.user,
        password: config.get('knex').connection.password
    }
});

const sqlStatement = 'CREATE DATABASE ' + config.get('knex').connection.database;

knex.raw(sqlStatement)
    .then(() => {
        console.log('Successfuly created', config.get('knex').connection.database, 'database');
    })
    .catch((err) => {
        console.log('Failed to create database', config.get('knex').connection.database);
        console.log(err);
    })
    .then(() => {
        knex.destroy();
        process.exit(0);
    });