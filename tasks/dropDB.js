const config = require('config');

// connect without database selected
var knex = require('knex')({
    client: config.get('knex').client,
    connection: config.get('knex').connection
});

const sqlStatement = 'DROP DATABASE ' + config.get('knex').connection.database;

knex.raw(sqlStatement)
    .then(() => {
        console.log('Successfuly dropped', config.get('knex').connection.database, 'database');
    })
    .catch((err) => {
        console.log('Failed to drop database', config.get('knex').connection.database);
        console.log(err);
    })
    .then(() => {
        knex.destroy();
        process.exit(0);
    });